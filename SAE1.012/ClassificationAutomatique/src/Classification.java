import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Classification extends Compteur {


    private static ArrayList<Depeche> lectureDepeches(String nomFichier) {
        //creation d'un tableau de dépêches
        ArrayList<Depeche> depeches = new ArrayList<>();
        try {
            // lecture du fichier d'entrée
            FileInputStream file = new FileInputStream(nomFichier);
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                String ligne = scanner.nextLine();
                String id = ligne.substring(3);
                ligne = scanner.nextLine();
                String date = ligne.substring(3);
                ligne = scanner.nextLine();
                String categorie = ligne.substring(3);
                ligne = scanner.nextLine();
                String lignes = ligne.substring(3);
                while (scanner.hasNextLine() && !ligne.equals("")) {
                    ligne = scanner.nextLine();
                    if (!ligne.equals("")) {
                        lignes = lignes + '\n' + ligne;
                    }
                }
                Depeche uneDepeche = new Depeche(id, date, categorie, lignes);
                depeches.add(uneDepeche);
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return depeches;
    }


    public static void classementDepeches(ArrayList<Depeche> depeches, ArrayList<Categorie> categories, String nomFichier) {


        try {
            FileWriter file = new FileWriter(nomFichier);

            int i = 0;
            while (i < depeches.size()) {
                ArrayList<PaireChaineEntier> résultat = new ArrayList<>();
                for (int j = 0; j < categories.size(); j++) {
                    int result = categories.get(j).score(depeches.get(i)); // résultat pour la dépêche n°i
                    résultat.add(new PaireChaineEntier(categories.get(j).getNom(), result));
                }

                // affichage de la catégorie qui est le plus représenté :
                if (i + 1 < 10) {
                    file.write("00" + (i + 1) + ":" + UtilitairePaireChaineEntier.chaineMax(résultat) + "\n");
                } else if (i + 1 < 100) {
                    file.write("0" + (i + 1) + ":" + UtilitairePaireChaineEntier.chaineMax(résultat) + "\n");
                } else {
                    file.write((i + 1) + ":" + UtilitairePaireChaineEntier.chaineMax(résultat) + "\n");
                }
                i++;
            }
            file.write("\n");


            // lecture du fichier d'entrée
            FileInputStream file1 = new FileInputStream(nomFichier);
            Scanner scanner = new Scanner(file1);
            String ligne = scanner.nextLine();

            i = 0;
            int j = 0;
            int max = 100;
            float moyenne = 0;
            while (i < categories.size() && !ligne.equals("")) {
                int compteur = 0;
                while (j < max && scanner.hasNextLine()) {
                    String[] words = ligne.split(":");
                    if (depeches.get(j).getCategorie().compareTo(words[1]) == 0) {
                        compteur++;
                    }
                    j++;
                    ligne = scanner.nextLine();
                }
                max += 100;

                moyenne += compteur;
                file.write(categories.get(i).getNom() + ":" + compteur + "%" + "\n");
                i++;
            }
            file.write("MOYENNE " + moyenne / categories.size() + "%" + "\n");

            file.close();
            scanner.close();
            System.out.println("Classement terminé voir fichier : " + nomFichier);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public static ArrayList<PaireChaineEntier> initDico(ArrayList<Depeche> depeches, String categorie) {
        ArrayList<PaireChaineEntier> resultat = new ArrayList<>();

        int i = 0;

        // on atteint la catégorie
        while (i < depeches.size() && depeches.get(i).getCategorie().compareTo(categorie) != 0) {
            i++;
        }


        // on est dans la catégorie
        while (i < depeches.size() && depeches.get(i).getCategorie().compareTo(categorie) == 0) {
            int j = 1;
            // on parcours le vecteur de mot
            while (j < depeches.get(i).getMots().size()) {
                int k = 0;
                // on compare le mot à chaque élément du lexique
                while (k < resultat.size() && resultat.get(k).getChaine().compareTo(depeches.get(i).getMots().get(j)) != 0) {
                    k++;
                }
                // on ajoute le mot au lexique s'il n'est pas déjà à l'intérieur et taille de 5 ou +
                if (k == resultat.size() && depeches.get(i).getMots().get(j).length() > 4) {
                    resultat.add(new PaireChaineEntier(depeches.get(i).getMots().get(j), 0));
                }
                j++;
            }
            i++;
        }
        triFusion(resultat, 0, resultat.size() - 1);
        return resultat;

    }

    public static void calculScores(ArrayList<Depeche> depeches, String categorie, ArrayList<PaireChaineEntier> dictionnaire) {

        int i = 0;
        // parcours toutes les depeches
        while (i < depeches.size()) {
            int j = 0;
            Depeche depCour = depeches.get(i);
            // parcours tous les mots de chaque depeche
            while (j < depCour.getMots().size()) {

                // RECHERCHE SEQUENTIELLE NON TRIEE
//                int k = 0;
//                // pour chaque mot, on le compare aux éléments de dico
//                while (k < dictionnaire.size() && dictionnaire.get(k).getChaine().compareTo(depCour.getMots().get(j)) != 0) {
//                    // on avance
//                    k++;
//                }
//                // si dans le dico et catégorie = catégorie => +1
//                if (k != dictionnaire.size() && depCour.getCategorie().compareTo(categorie) == 0) {
//                    dictionnaire.get(k).addEntier();
//                    // si pas dans le dico catégorie  catégorie => -1
//                } else if (k != dictionnaire.size() && depCour.getCategorie().compareTo(categorie) != 0) {
//                    dictionnaire.get(k).subEntier();
//                }

                int inf = 0;
                int sup = dictionnaire.size() - 1;
                int k = -1; // le mot n'est pas encore trouvé

                while (inf <= sup && k == -1) {
                    int m = (inf + sup) / 2;
                    if (dictionnaire.get(m).getChaine().compareTo(depCour.getMots().get(j)) == 0) {
                        // le mot est trouvé
                        k = m;
                        // on incrémente le compteur
                    } else if (dictionnaire.get(m).getChaine().compareTo(depCour.getMots().get(j)) < 0) {
                        // le mot est après de m
                        inf = m + 1;
                        // on incrémente le compteur
                    } else {
                        // le mot est avant m
                        sup = m - 1;
                    }
                }
                if (k != -1 && depCour.getCategorie().compareTo(categorie) == 0) {
                    dictionnaire.get(k).addEntier();
                    // on incrémente le compteur
                } else if (k != -1 && depCour.getCategorie().compareTo(categorie) != 0) {
                    dictionnaire.get(k).subEntier();
                    // on incrémente le compteur
                }
                // on avance
                j++;
            }
            // on avance
            i++;
        }
    }


    public static int poidsPourScore(int score) {

        if (score <= 0) {
            return 0;
        } else if (score == 1) {
            return 1;
        } else if (score == 2) {
            return 2;
        }else {
            return 3;
        }
    }

    public static void generationLexique(ArrayList<Depeche> depeches, String categorie, String nomFichier) {

        try {
            FileWriter file = new FileWriter(nomFichier);

            ArrayList<PaireChaineEntier> dico = initDico(depeches, categorie);
            calculScores(depeches, categorie, dico);

            for (int i = 0; i < dico.size(); i++) {
                file.write(dico.get(i).getChaine() + ":" + poidsPourScore(dico.get(i).getEntier()) + "\n");
            }

            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void triFusion(ArrayList<PaireChaineEntier> dico, int inf, int sup) {
        // { 0<=inf<=sup<vInt.size(), vInt[inf..sup] quelconque } =>
        // { * vInt[inf..sup] a été trié
        // * résultat = nombre de comparaisons entre 2 éléments de vInt[inf..sup]
        if (inf < sup) {
            int m = (inf + sup) / 2;
            triFusion(dico, inf, m);
            triFusion(dico, m + 1, sup);
            fusionTabGTabD(dico, inf, m, sup);
        }
    }

    public static void fusionTabGTabD(ArrayList<PaireChaineEntier> dico, int inf, int m, int sup) {
        ArrayList<PaireChaineEntier> dicoTemp = new ArrayList<>();

        int compteur = 0;
        int i = inf;
        int j = m + 1;

        while (i <= m && j <= sup) {
            if (dico.get(i).getChaine().compareTo(dico.get(j).getChaine()) <= 0) {
                dicoTemp.add(dico.get(i));
                i++;
            } else {
                dicoTemp.add(dico.get(j));
                j++;
            }
            compteur++;
        }

        // Add remaining elements from the left and right halves
        while (i <= m) {
            dicoTemp.add(dico.get(i));
            i++;
        }

        while (j <= sup) {
            dicoTemp.add(dico.get(j));
            j++;
        }

        // Copy the sorted elements back to the original ArrayList
        for (int k = 0; k < dicoTemp.size(); k++) {
            dico.set(inf + k, dicoTemp.get(k));
        }
    }

    public static void main(String[] args) {

        ArrayList<Depeche> depeches_init = lectureDepeches("./depeches.txt");
        ArrayList<Depeche> depeches_test = lectureDepeches("./test.txt");

        Scanner lecteur = new Scanner(System.in);
        System.out.println("\nBonjour, " + "Monsieur Blanchon, " + "bienvenue dans le programme de classification de dépêches.\n");
        System.out.print("Quelles dépêches voulez-vous classer ? (depeches.txt / test.txt) ");
        String nomFichier = lecteur.nextLine();

        boolean rejouer = true;

        while (nomFichier.compareTo("depeches.txt") != 0 && nomFichier.compareTo("test.txt") != 0 && rejouer) {
            System.out.println("Erreur, le fichier n'existe pas.");
            System.out.print("Voulez-vous re essayer ? (oui / non) ");
            String reponse = lecteur.nextLine();
            if (reponse.compareTo("oui") != 0) {
                rejouer = false;
            } else {
                System.out.print("Quelles dépêches voulez-vous classer ? (depeches.txt / test.txt) ");
                nomFichier = lecteur.nextLine();
            }
        }

        while(rejouer) {

            if (rejouer) {
                System.out.println("Vous avez choisit " + nomFichier + " comme fichier de dépêches.\n");
                System.out.println("Chargement des dépêches...");
                ArrayList<Depeche> depeches_nomFichier = lectureDepeches("./" + nomFichier);
                System.out.println("Dépêches chargées avec succès !\n");
                System.out.print("Indiquez vers quel fichier vous voulez écrire le résultat : ");
                String fichierSortie = lecteur.nextLine();


                if (nomFichier.compareTo("depeches.txt") == 0) {

                    long startTime = System.currentTimeMillis();

                    ArrayList<Categorie> categories_auto = new ArrayList<>(Arrays.asList(new Categorie("ENVIRONNEMENT-SCIENCES"), new Categorie("CULTURE"), new Categorie("ECONOMIE"),
                            new Categorie("POLITIQUE"), new Categorie("SPORTS")));  // création d'une liste qui recensse toutes les catégories

                    generationLexique(depeches_init, "ENVIRONNEMENT-SCIENCES", "./environnement-sciences_automat");
                    categories_auto.get(0).initLexique("./environnement-sciences_automat");
                    generationLexique(depeches_init, "CULTURE", "./culture_automat");
                    categories_auto.get(1).initLexique("./culture_automat");
                    generationLexique(depeches_init, "ECONOMIE", "./economie_automat");
                    categories_auto.get(2).initLexique("./economie_automat");
                    generationLexique(depeches_init, "POLITIQUE", "./politique_automat");
                    categories_auto.get(3).initLexique("./politique_automat");
                    generationLexique(depeches_init, "SPORTS", "./sports_automat");
                    categories_auto.get(4).initLexique("./sports_automat");

                    System.out.println("Dépêches chargées avec succès !\n");
                    System.out.println("Classement des dépêches...");
                    classementDepeches(depeches_init, categories_auto, fichierSortie);

                    long endTime = System.currentTimeMillis();
                    System.out.println("-----------------------------------------");
                    System.out.println("Le temps total d'exécution : " + (endTime - startTime) + "ms");
                    System.out.println("-----------------------------------------");

                } else {
                    long startTime = System.currentTimeMillis();

                    ArrayList<Categorie> categories_auto = new ArrayList<>(Arrays.asList(new Categorie("ENVIRONNEMENT-SCIENCES"), new Categorie("CULTURE"), new Categorie("ECONOMIE"),
                            new Categorie("POLITIQUE"), new Categorie("SPORTS")));  // création d'une liste qui recensse toutes les catégories

                    generationLexique(depeches_init, "ENVIRONNEMENT-SCIENCES", "./environnement-sciences_automat");
                    categories_auto.get(0).initLexique("./environnement-sciences_automat");
                    generationLexique(depeches_init, "CULTURE", "./culture_automat");
                    categories_auto.get(1).initLexique("./culture_automat");
                    generationLexique(depeches_init, "ECONOMIE", "./economie_automat");
                    categories_auto.get(2).initLexique("./economie_automat");
                    generationLexique(depeches_init, "POLITIQUE", "./politique_automat");
                    categories_auto.get(3).initLexique("./politique_automat");
                    generationLexique(depeches_init, "SPORTS", "./sports_automat");
                    categories_auto.get(4).initLexique("./sports_automat");


                    System.out.println("Dépêches chargées avec succès !\n");
                    System.out.println("Classement des dépêches...");
                    classementDepeches(depeches_test, categories_auto, fichierSortie);

                    long endTime = System.currentTimeMillis();
                    System.out.println("-----------------------------------------");
                    System.out.println("Le temps total d'exécution : " + (endTime - startTime) + "ms");
                    System.out.println("-----------------------------------------");
                }
                System.out.print("\nVoulez-vous refaire avec un autre fichier ? (oui / non) ");
                String reponse = lecteur.nextLine();
                if (reponse.compareTo("oui") != 0) {
                    rejouer = false;
                } else {
                    System.out.print("Quelle dépêche voulez-vous classer ? (depeches.txt / test.txt) ");
                    nomFichier = lecteur.nextLine();
                }
            }
        }

        System.out.println("\nAu revoir Monsieur Blanchon (20/20)!");




    }





}






//        long startTime = System.currentTimeMillis();
//
//        long endTime = System.currentTimeMillis();
//        System.out.println("Le temps total d'exécution : " + (endTime - startTime) + "ms");


