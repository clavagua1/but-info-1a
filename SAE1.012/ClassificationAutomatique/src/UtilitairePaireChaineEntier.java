import java.util.ArrayList;

public class UtilitairePaireChaineEntier extends Compteur {


    public static int indicePourChaine(ArrayList<PaireChaineEntier> listePaires, String chaine) {

        int i = 0;
        // tant que i inférieur à la taille de la liste
        // et que l'élément en position i est différent de chaine alors on avance
        while (i < listePaires.size() && listePaires.get(i).getChaine().compareTo(chaine) != 0) {
            i++; // j'avance
        }

        if (i == listePaires.size()) { // cas où la chaine n'est paas présente dans la liste
            return -1;
        } else {
            return i;
        }

    }

    public static int entierPourChaine(ArrayList<PaireChaineEntier> listePaires, String chaine) {
        // => retourne l'entier associé à la chaine de caractères chaine dans listePaires si elle est présente et 0 sinon


        if (listePaires.get(listePaires.size() - 1).getChaine().compareTo(chaine) < 0) { // v.[v.size()-1] < val
            return 0;
        } else {
            int inf = 0;
            int sup = listePaires.size() - 1;
            int m;
            while (inf < sup) {
                m = (inf + sup) / 2;
                if (listePaires.get(m).getChaine().compareTo(chaine) >= 0) {
                    sup = m;
                } else {
                    inf = m + 1;
                }
                compteur++;
            }
            compteur++;
            if (listePaires.get(sup).getChaine().compareTo(chaine) == 0) {
                compteur++;
                return listePaires.get(sup).getEntier();
            } else {
                return 0;
            }
        }


//            int i = 0;
//            while (i < listePaires.size() && listePaires.get(i).getChaine().compareTo(chaine) != 0) {
//                // on incrémente le compteur
//                compteur++;
//                i++;
//            }
//
//            if (i == listePaires.size()) {
//                // on incrémente le compteur
//                compteur++;
//                return 0;
//            } else {
//                return listePaires.get(i).getEntier();
//            }


    }

        public static String chaineMax (ArrayList < PaireChaineEntier > listePaires) {

            PaireChaineEntier chaineMax = listePaires.get(0);
            for (int i = 1; i < listePaires.size(); i++) {
                if (chaineMax.getEntier() < listePaires.get(i).getEntier()) {
                    chaineMax = listePaires.get(i);
                }
            }
            if (chaineMax.getEntier() == 0) {
                return "TEST";
            } else {
                return chaineMax.getChaine();
            }
        }


        public static float moyenne (ArrayList < PaireChaineEntier > listePaires) {

            float resultat = 0.0f;

            for (int i = 0; i < listePaires.size(); i++) {
                resultat += listePaires.get(i).getEntier();
            }

            return resultat / listePaires.size();
        }

    }
