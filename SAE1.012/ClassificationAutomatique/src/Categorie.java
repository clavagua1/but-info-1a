import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Categorie extends Compteur {

    private String nom; // le nom de la catégorie p.ex : sport, politique,...
    private ArrayList<PaireChaineEntier> lexique ; //le lexique de la catégorie

    // constructeur
    public Categorie(String nom) {
        this.nom = nom;
    }


    public String getNom() {
        return nom;
    }


    public  ArrayList<PaireChaineEntier> getLexique() {
        return lexique;
    }


    // initialisation du lexique de la catégorie à partir du contenu d'un fichier texte
    public void initLexique(String nomFichier) {

        //creation d'un tableau de nomFichier
        ArrayList<PaireChaineEntier> nomfichier = new ArrayList<>();
        try {
            // lecture du fichier d'entrée
            FileInputStream file = new FileInputStream(nomFichier);
            Scanner scanner = new Scanner(file);
            String ligne = scanner.nextLine();

            while (scanner.hasNextLine() && !ligne.equals("")) {
                String[] words = ligne.split(":");
                PaireChaineEntier unMot = new PaireChaineEntier(words[0], parseInt(words[1]));
                nomfichier.add(unMot);
                ligne = scanner.nextLine();
            }
            Classification.triFusion(nomfichier,0, nomfichier.size()-1);
            lexique = nomfichier;
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //calcul du score d'une dépêche pour la catégorie
    public int score(Depeche d) {
        ArrayList<String> tabMots = d.getMots();
        int nbPoints = 0;

        int i = 0;
        // on parcours tous les mots
        while(i < tabMots.size()){
            // on ajoute le nombre de points associé au mot regardé
            nbPoints = nbPoints + UtilitairePaireChaineEntier.entierPourChaine(getLexique(), tabMots.get(i));
            // on avance
            i++;
        }

        return nbPoints;
    }


}
